#include <grpcpp/grpcpp.h>
#include <string>
#include "sum.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using sum::sumResponse;
using sum::sumRequest;
using sum::Sum;

// Server Implementation
class sumServiceImplementation final : public Sum::Service {
  Status sumNumbers(ServerContext* context, const sumRequest* request,
                     sumResponse* reply) override {
    
    int sum = request->a() + request->b() + request->c();
    reply->set_sum(sum);
    /*
    std:: int32 operation(a,b,c);
    reply->set_sum(operation + request->a() + request->b() + request->c());
    */
    return Status::OK;
  }
};

void RunServer() {
  std::string server_address("0.0.0.0:50052");
  sumServiceImplementation service;

  ServerBuilder builder;
  // Listen on the given address without any authentication mechanism
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which
  // communication with client takes place
  builder.RegisterService(&service);

  // Assembling the server
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on port: " << server_address << std::endl;

  server->Wait();
}

int main(int argc, char** argv) {
  RunServer();
  return 0;
}